package pw.freshek.gmstoolbox.utils;

import java.io.IOException;
import java.io.InputStream;

public class StreamUtils {
    public static String readStreamToString(InputStream stream) throws IOException {
        StringBuilder s = new StringBuilder();

        byte[] buffer = new byte[1024];
        int cnt;

        while ((cnt = stream.read(buffer)) > 0) {
            s.append(new String(buffer, 0, cnt));
        }

        return s.toString();
    }
}
