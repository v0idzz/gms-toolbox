package pw.freshek.gmstoolbox.su;

import android.text.TextUtils;
import pw.freshek.gmstoolbox.utils.StreamUtils;

import java.io.File;
import java.io.OutputStream;

public class SUHelper {

    public static void copyFile(File file, File destination, ICommandExecutionHandler<String> handler) {
        final String copyCmd = "cp";
        executeCommand(new String[]{copyCmd, file.getAbsolutePath(), destination.getAbsolutePath()}, handler);
    }

    public static void executeCommand(String command, ICommandExecutionHandler<String> handler) {
        new Thread(() -> {

            String result;
            try {
                Process process = Runtime.getRuntime().exec("su -c " + command);

                if (StreamUtils.readStreamToString(process.getErrorStream()).length() != 0)
                    throw new SUException();

                OutputStream outputStream = process.getOutputStream();
                outputStream.write(command.getBytes());
                outputStream.flush();
                outputStream.close();

                result = StreamUtils.readStreamToString(process.getInputStream());

                process.waitFor();

            } catch (Exception e) {
                handler.onFailure(e);
                return;
            }

            handler.onSuccess(result);
        }).start();
    }

    public static void executeCommand(String[] command, ICommandExecutionHandler<String> handler) {
        executeCommand(constructCommand(command), handler);
    }

    private static String constructCommand(String[] params) {
        return TextUtils.join(" ", params);
    }
}
