package pw.freshek.gmstoolbox.su;

public class CommandExecutionFailedException extends Exception {
    private Exception innerException;

    public Exception getInnerException() {
        return innerException;
    }

    public CommandExecutionFailedException(Exception e) {
        innerException = e;
    }
}
