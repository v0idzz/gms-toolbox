package pw.freshek.gmstoolbox.su;

public interface ICommandExecutionHandler<T> {
    void onFailure(Exception e);
    void onSuccess(T result);
}
