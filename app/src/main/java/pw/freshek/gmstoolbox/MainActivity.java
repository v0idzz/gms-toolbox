package pw.freshek.gmstoolbox;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import pw.freshek.gmstoolbox.su.ICommandExecutionHandler;
import pw.freshek.gmstoolbox.su.SUHelper;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private final String ANDROID_PAY_DATABASE_PATH = "/data/data/com.google.android.gms/databases/android_pay";
    private final String GMS_DATA_PATH = "/data/data/com.google.android.gms";

    TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultView = findViewById(R.id.text_attestation_result);
    }

    public void clickOnButtonCheck(View view) {
        try {
            File tempDb = File.createTempFile("android_pay", ".db", this.getCacheDir());
            tempDb.deleteOnExit();

            SUHelper.copyFile(new File(ANDROID_PAY_DATABASE_PATH), tempDb, new ICommandExecutionHandler<String>() {
                @Override
                public void onFailure(Exception e) {
                    showQuickToast("Couldn't copy android_pay database!");
                }

                @Override
                public void onSuccess(String result) {
                    if (!result.equals("")) {
                        showQuickToast(result);
                        return;
                    }

                    boolean fails;
                    try {
                        SQLiteDatabase db = SQLiteDatabase.openDatabase(tempDb.getAbsolutePath(), null, SQLiteDatabase.OPEN_READWRITE);
                        Cursor resultSet = db.rawQuery("SELECT * FROM Wallets WHERE fails_attestation = 1 LIMIT 1", null);
                        fails = resultSet.getCount() != 0;

                        resultSet.close();
                    } catch (Exception ignored) {
                        showQuickToast("Couldn't query the android_pay database!");
                        return;
                    }

                    boolean finalFails = fails;
                    runOnUiThread(() -> {
                        if (!finalFails)
                            resultView.setText("OK");
                        else
                            resultView.setText("Fails");
                    });
                }
            });
        } catch (Exception e) {
            showQuickToast("Something went terribly wrong...");
        }
    }

    public void clickOnButtonDeleteReboot(View view) {
    }

    private void showQuickToast(String msg) {
        runOnUiThread(() -> Toast.makeText(this, msg, Toast.LENGTH_SHORT).show());
    }
}
